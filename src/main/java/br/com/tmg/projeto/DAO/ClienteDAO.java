
package br.com.tmg.projeto.DAO;

import br.com.tmg.projeto.model.Cliente;
import java.util.List;
import javax.persistence.NoResultException;
import javax.persistence.Query;


public class ClienteDAO extends DAO<Cliente>{
    
    public ClienteDAO() {
        super(Cliente.class);
    }

    public List<Cliente> findByFiltro(String id, String nome) {
        this.em = JPAUtil.getEntityManager();
        List<Cliente> lista;
        em.getTransaction().begin();

        StringBuilder sql = new StringBuilder("from Cliente a where 1=1 ");

        if (id != null && !id.isEmpty()) {
            sql.append(" and a.id = :Id ");
        }

        if (nome != null && !nome.isEmpty()) {
            sql.append(" and a.nome like :Nome ");
        }
        
         Query query = em.createQuery(sql.toString());
        
        if (id!= null && !id.isEmpty()) {
           query.setParameter("Id", new Long(id));
        }

        if (nome != null && !nome.isEmpty()) {
            query.setParameter("Nome", "%" + nome + "%");
        }

        lista = query.getResultList();

        em.getTransaction().commit();
        em.close();

        return lista;
    }
    
    
    public Cliente getCliente (String email, String senha) {
  
            try {
                  Cliente cliente = (Cliente) em
                             .createQuery(
                                         "SELECT u from Cliente u where u.email = :name and u.senha = :senha")
                             .setParameter("name", email)
                             .setParameter("senha", senha).getSingleResult();
  
                  return cliente;
            } catch (NoResultException e) {
                  return null;
            }
      }
    
    
    public Cliente findByUsername (String userName){
        
         Cliente cliente = new Cliente(); 
         try{
         this.em = JPAUtil.getEntityManager();         
        
         Query query = this.em.createQuery("from Cliente a where a.nome = :userName ") ; 
         query.setParameter("userName", userName) ; 
         cliente = (Cliente)query.getSingleResult() ; 
         
         
         this.em.close();
         }catch(NoResultException ex){
             ex.printStackTrace();
             return cliente ; 
         }
         
        return cliente ; 
    }

    
    
    
    
}
