
package br.com.tmg.projeto.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Item_compra")
public class Item extends Entidade{
    
    @JoinColumn(name = "idLivro", referencedColumnName = "id")
    @ManyToOne(optional = false)
     private Livro livro;
    
    @JoinColumn(name = "idCompra", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Compra compra;
    
    @Column(nullable = false , length = 3)
    private int quantidade;
    
    @Column(nullable = false , length = 8)
    private double preco;

    public Item() {
    }

    
    public Item(Livro livro, int quantidade, double preco) {
        this.livro = livro;
        this.quantidade = quantidade;
        this.preco = preco;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public Livro getLivro() {
        return livro;
    }

    public void setLivro(Livro livro) {
        this.livro = livro;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
    
    public double getTotal(){
        return this.quantidade * this.preco;
    }

    public Compra getCompra() {
        return compra;
    }

    public void setCompra(Compra compra) {
        this.compra = compra;
    }
    
}
