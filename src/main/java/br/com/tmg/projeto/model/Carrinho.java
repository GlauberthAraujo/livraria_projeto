
package br.com.tmg.projeto.model;

import java.util.ArrayList;

public class Carrinho {
    
    private ArrayList<Item> itens ;

    public Carrinho() {
        this.itens = new ArrayList<>();
    }

    public  ArrayList<Item> getItens() {
        return itens;
    }

    public void setItens(ArrayList<Item> itens) {
        this.itens = itens;
    }

    public void adicionarItem(Livro livro, int quantidade, double preco){
        this.itens.add(new Item(livro, quantidade, preco));
    }
  
    public void removerItem(int posicao){
        this.itens.remove(posicao);
    }
    
    public double getTotal(){
        double total = 0 ;
        
        for(Item item : this.itens){
            total += item.getTotal();
        }
        
        
        return total;
    }
}
