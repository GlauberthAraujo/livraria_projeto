package br.com.tmg.projeto.bean;

import br.com.tmg.projeto.DAO.ClienteDAO;
import br.com.tmg.projeto.model.Cliente;
import java.io.Serializable;
import java.util.List;
import javax.faces.view.ViewScoped;

import javax.inject.Named;

@Named(value = "clienteBean")
@ViewScoped
public class ClienteBean extends Bean implements Serializable {

    private Cliente clienteSelecionado;

    private Cliente cliente;
    private ClienteDAO dao;

    private String nome;
    private String id;
    
    private List<Cliente> lista;
   

    public ClienteBean() {
        this.cliente = new Cliente();
        this.dao = new ClienteDAO();
    }

    public void salvar() {

        if (this.cliente.getId() == 0) {
            dao.save(cliente);
            this.addMessageInfo("Cliente salvo com sucesso!");

        } else {
            dao.update(cliente);
            this.addMessageInfo("Cliente alterado com sucesso!");
        }

    }

    public void editar() {

        if (this.clienteSelecionado.getId() == 0) {
            this.dao.save(clienteSelecionado);
            this.addMessageInfo("Cliente salvo com sucesso!");
        } else {
            this.dao.update(clienteSelecionado);
            this.addMessageInfo("Cliente alterado com sucesso!");
        }

        this.pesquisa();
    }

    public void pesquisa() {

        try {
            this.lista = this.dao.findByFiltro(id, nome);

        } catch (Exception ex) {
            ex.printStackTrace();

        }

    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public void novo() {
        this.cliente = new Cliente();
    }

    /*
     public static void main(String[] args) {
        
         Cliente c = new Cliente();
         ClienteDAO dao = new ClienteDAO();
         
         c.setNome("Glauberth");         
         c.setNascimento("00000000");
         c.setEndereco("Rua B");
         c.setEmail("c@gmail.com.br");
         c.setSenha("12345678");
         c.setCpf("1245987451");
         dao.save(c);
         
         
    }
     */
    public Cliente getClienteSelecionado() {
        return clienteSelecionado;
    }

    public void setClienteSelecionado(Cliente clienteSelecionado) {
        this.clienteSelecionado = clienteSelecionado;
    }

    public List<Cliente> getLista() {
        return lista;
    }

    public void setLista(List<Cliente> lista) {
        this.lista = lista;
    }
   

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
