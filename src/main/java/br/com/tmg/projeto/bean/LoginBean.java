package br.com.tmg.projeto.bean;

import br.com.tmg.projeto.DAO.ClienteDAO;
import br.com.tmg.projeto.DAO.UsuarioDAO;
import br.com.tmg.projeto.model.Cliente;
import br.com.tmg.projeto.model.Usuario;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import javax.validation.ConstraintViolationException;

@Named(value = "loginBean")
@SessionScoped
public class LoginBean extends Bean {

    private Cliente cliente = new Cliente();
    private Cliente clienteLogado;
    private ClienteDAO dao;
    private List<Cliente> lista;
    
    private String nome;
    private String id;
    
    
    private boolean logado;
    
    
    private Usuario usuario = new Usuario();    
    private UsuarioDAO userDao;
    private Usuario usuarioLogado;
    

    public LoginBean() {
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String toHome() {

        dao = new ClienteDAO();

        clienteLogado = dao.findByUsername(cliente.getNome());

        logado = this.cliente.getSenha().equals(clienteLogado.getSenha());
        if (logado) {

            FacesContext facesContext = FacesContext.getCurrentInstance();
            ExternalContext context = facesContext.getExternalContext();
            HttpSession session = (HttpSession) context.getSession(false);
            session.setAttribute("cliente", clienteLogado);

            return "Home.xhtml";

        }

        return null;

    }

    public String restrito() {

        userDao = new UsuarioDAO();
        usuarioLogado = userDao.findByUsername(usuario.getNome());

        logado = this.usuario.getSenha().equals(usuarioLogado.getSenha());

        if (logado) {
            
            FacesContext facesContext = FacesContext.getCurrentInstance();
            ExternalContext context = facesContext.getExternalContext();
            HttpSession session = (HttpSession) context.getSession(false);
            session.setAttribute("usuario", usuarioLogado);

            return "index.xhtml";
        }

        return null;
    }

    public void editarCliente() {

        if (this.clienteLogado.getId() == 0) {
            this.dao.save(clienteLogado);
            this.addMessageInfo("Salvo com sucesso!");
        } else {
            this.dao.update(clienteLogado);
            this.addMessageInfo("Alterado com sucesso!");
        }

        this.pesquisa();
    }
    
    public void pesquisa() {

        try {
            this.lista = this.dao.findByFiltro(id, nome);

        } catch (Exception ex) {
            ex.printStackTrace();

        }
    }  
    
     public void deletar(Usuario usuario) {

        try {

            this.dao.delete(clienteLogado);
            this.addMessageInfo("Removido com sucesso!");

        } catch (Exception ex) {
            if (ex.getCause() instanceof ConstraintViolationException) {
                this.addMessageErro("Não é possível os dados cadastrados");
            } else {
                this.addMessageErro("Falha ao remover Cliente");
                ex.printStackTrace();
            }
        }
    }    
    
    public Cliente getClienteLogado() {
        return clienteLogado;
    }

    public void setClienteLogado(Cliente clienteLogado) {
        this.clienteLogado = clienteLogado;
    }
    

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Usuario getUsuarioLogado() {
        return usuarioLogado;
    }

    public void setUsuarioLogado(Usuario usuarioLogado) {
        this.usuarioLogado = usuarioLogado;
    }

}
