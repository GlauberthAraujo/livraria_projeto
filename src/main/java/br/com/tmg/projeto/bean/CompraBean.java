
package br.com.tmg.projeto.bean;

import br.com.tmg.projeto.model.Compra;
import br.com.tmg.projeto.model.Item;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;


@Named(value = "compraBean")
@SessionScoped
public class CompraBean implements Serializable{
    
     private Item item ;
     private Compra compra;
     private List<Item> carrinho;
     private double total;
     
    public CompraBean() {
    }


    
    public String adicionarAoCarrinho() {  
         
        carrinho.add(item);  
        total += (this.item.getLivro().getPrecoNovo()
                * this.item.getQuantidade());  
        item = new Item();  
          
        return "venda";  
    }  
  
    public String limparCampos() {  
        item = new Item();          
        compra = new Compra();  
        carrinho.clear();  
        return "venda";  
  
    }  
  
    public String removerCarrinho(Item item) {  
        carrinho.remove(item);  
        total -= (this.item.getLivro().getPrecoNovo() 
                *this.item.getQuantidade());  
        return "venda";  
    }  
  
    public String carregarCarrinho() {  
        carrinho.listIterator();  
        return "carrinho";  
    }
    
      /*
    public String finalizarVenda() {  
        CompraDAO dao = new CompraDAO();  
        dao.cadastrarVenda(compra);  
        compra = new Compra();  
          
          
        return "venda";  
    }  
    */

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Compra getCompra() {
        return compra;
    }

    public void setCompra(Compra compra) {
        this.compra = compra;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public List<Item> getCarrinho() {
        return carrinho;
    }

    public void setCarrinho(List<Item> carrinho) {
        this.carrinho = carrinho;
    }

    
    
}
