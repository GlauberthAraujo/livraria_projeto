
package br.com.tmg.projeto.filtro;

import br.com.tmg.projeto.model.Cliente;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebFilter(filterName = "UsuarioFilter", urlPatterns = {"/AcessoRestrito/*"})
public class UsuarioFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        
            HttpServletRequest req = (HttpServletRequest) request  ;
            HttpServletResponse res = (HttpServletResponse) response  ;
        
        
              HttpSession session =  req.getSession();
              Cliente cliente  = (Cliente)(session.getAttribute("cliente")) ; 
            
            if(cliente != null ){
              // res.sendRedirect("http://www.google.com.br");
            }
        
        
        
          chain.doFilter(request, response);
        
    }

    @Override
    public void destroy() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    

  
    
}
